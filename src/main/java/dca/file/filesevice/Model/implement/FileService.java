package dca.file.filesevice.Model.implement;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import dca.file.filesevice.Model.Service.IFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;
@Service
public class FileService  implements IFileService {

    private Logger logger = LoggerFactory.getLogger(FileService.class);

    @Autowired
   private AmazonS3 s3Client;

    @Value("${aws.s3.bucket}")
    private String bucket;

    @Value("${aws.s3.endpointurl}")
    private String pathFile;



    @Override
    public ResponseEntity<byte[]> loadFile(String nombre) throws IOException {
        byte[] bytes = getFile(nombre);
        String fileName = fileName(nombre);

        if(bytes == null && bytes.length <= 0 ){
            bytes = getFile("no_content.png");
            fileName = "no_content.png";
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        httpHeaders.setContentLength(bytes.length);
        httpHeaders.setContentDispositionFormData("attachment", fileName);

        return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);

    }

    @Override
    public String copyfile(MultipartFile file) throws AmazonServiceException, IOException {

        File f = ConvertMultipartToFile(file);
        String nombreArchivo = generateFileName(file);

        uploadFileToS3bucket(nombreArchivo, f);
        f.delete();

        return nombreArchivo;
    }

    @Override
    public Boolean deleteFile(String nombre) throws IOException {
        if(nombre != null && nombre.length() > 0){
            byte[] bytes = getFile(nombre);

            if(bytes != null && bytes.length > 0){

                String archivoFotoAnterior = fileName(nombre);
                System.out.println(deleteFileFromS3bucket(archivoFotoAnterior));
                return true;

            }
        }

        return false;
    }

    @Override
    public Path route(String nombre) {
        return Paths.get(pathFile).resolve(nombre).toAbsolutePath();
    }

    @Override
    public File ConvertMultipartToFile(MultipartFile file) throws IOException {
        File convertFile = new File(file.getOriginalFilename());
        FileOutputStream stream = new FileOutputStream(convertFile);
        stream.write(file.getBytes());
        stream.close();
        return convertFile;
    }

    @Override
    public String generateFileName(MultipartFile file) {
        return  file.getOriginalFilename().replace(" ","");
    }

    @Override
    public void uploadFileToS3bucket(String fileName, File file) {
        try {
        s3Client.putObject(new PutObjectRequest(bucket, fileName, file));

        }catch (AmazonServiceException ase) {
            logger.info("Caught an AmazonServiceException from PUT requests, rejected reasons:");
            logger.info("Error Message:    " + ase.getMessage());
            logger.info("HTTP Status Code: " + ase.getStatusCode());
            logger.info("AWS Error Code:   " + ase.getErrorCode());
            logger.info("Error Type:       " + ase.getErrorType());
            logger.info("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException: ");
            logger.info("Error Message: " + ace.getMessage());
        }

    }



    @Override
    public String deleteFileFromS3bucket(String fileName) {
        s3Client.deleteObject(new DeleteObjectRequest(bucket, fileName));
        return "Eliminado satisfactoriamente";
    }

    @Override
    public byte[] getFile(String fileName) throws IOException {
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucket, fileName);
        S3Object s3Object = s3Client.getObject(getObjectRequest);
        S3ObjectInputStream objectInputStream = s3Object.getObjectContent();
        byte[] bytes = IOUtils.toByteArray(objectInputStream);

        return bytes;
    }





    private String fileName(String name) throws UnsupportedEncodingException {
        return URLEncoder.encode(name, "UTF-8").replaceAll("\\+", "%20").replaceAll(" ","");
    }

}
