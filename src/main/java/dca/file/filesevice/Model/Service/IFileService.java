package dca.file.filesevice.Model.Service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public interface IFileService {
    ResponseEntity<byte[]> loadFile(String nombreFoto) throws IOException;
    String copyfile(MultipartFile archivo) throws IOException;
    Boolean deleteFile(String nombre) throws IOException;
    Path route(String nombre);
    File ConvertMultipartToFile(MultipartFile file) throws IOException;
    String generateFileName(MultipartFile file);
    void uploadFileToS3bucket(String fileName, File file);
    String deleteFileFromS3bucket(String fileName);
    byte[] getFile(String fileName) throws IOException;

}
