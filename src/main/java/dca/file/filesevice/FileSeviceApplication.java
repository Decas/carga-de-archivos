package dca.file.filesevice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//@EnableFeignClients
public class FileSeviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileSeviceApplication.class, args);
    }

}
