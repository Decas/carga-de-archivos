package dca.file.filesevice.Controller;

import dca.file.filesevice.Model.File;
import dca.file.filesevice.Model.Service.IFileService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
import java.util.stream.Collectors;


@CrossOrigin(origins = {"https://master.d3hkaezoyccihu.amplifyapp.com", "http://localhost:4200", "*"})
@RestController
@Api(tags = {"Carga Archivo"},protocols = "http, https", consumes = "application/json", produces = "application/json")
public class FileController {
    @Autowired
    private IFileService fileService;

    private List<String> lista=new ArrayList<>();


    @PostMapping("/horasExtras/upload")
    public ResponseEntity<?> upload(@RequestParam("files") MultipartFile[] files, @RequestParam("id") Long id) throws Exception {

        Map<String, Object> response = new HashMap<>();

        if (files.length>0) {

            String s=": ";
            try {


                for (int i = 0; i <files.length ; i++)
                {s+=", "+this.fileService.copyfile(files[i]);
                    this.lista.add(this.fileService.copyfile(files[i]));
                }


            } catch (IOException e) {
                response.put("mensaje", "Error al subir la imagen del cliente");
                response.put("error", e.getMessage().concat(": ").concat(e.getCause().getMessage()));
                return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }




           // response.put("archivo", files); esto no se usa pq devuelve un error de archivo no serializado
            response.put("mensaje", "Has subido correctamente los archivos "+s+" "+id);

        }
        return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
    }

    @GetMapping("/uploads/file/{nameFile:.+}")
    public ResponseEntity<byte[]> checkFile(@PathVariable String nameFile){

        ResponseEntity<byte[]> recurso = null;

        try {
            recurso =  this.fileService.loadFile(nameFile);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return recurso;

    }
    @GetMapping("/files")
    public ResponseEntity<?> listFiles() throws Exception {

        ResponseEntity<byte[]> recurso = null;

        if(!lista.isEmpty()) {
            try {
                for (String s : this.lista) {
                    recurso = this.fileService.loadFile(s);

                }

            } catch (IOException e) {
                e.printStackTrace();
                throw new Exception("aqui hay un eroor");
            }

        }

        /*recurso=file.getName().stream().map(f-> {
            try {
                this.fileService.loadFile(f);
            } catch (IOException e) {
                e.printStackTrace();

            }

        }).collect(Collectors.toList());*/

                return recurso;
    }


/*
    @DeleteMapping("/delete/file/{name:.+}/horaExtra/{id}")
    public String deleteFile(@PathVariable String name, @PathVariable Long id) throws Exception {
        if(this.fileService.deleteFile(name)){

           return "Eliminada la imagen satisfactoriamente";

        }

        return "Error en la eliminación";
    }

*/





}
